# Car Catalog Service

## Требования к Software

- OPENJDK 11

## Стек технологий
- Spring Boot MVC

- Spring Boot JPA

- Liquibase

- PostgreSQL

- Apache Maven

## Запуск приложения
Для запуска приложения, необходимо на сервере запустить car-catalog-service.bat.

## Endpoints

### Марки автомобилей
http://185.89.10.104:8004/brands

### Модели автомобилей заданной марки
http://185.89.10.104:8004/models/{brandId}

