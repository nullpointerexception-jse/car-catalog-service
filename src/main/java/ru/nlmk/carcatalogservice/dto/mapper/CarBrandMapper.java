package ru.nlmk.carcatalogservice.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.nlmk.carcatalogservice.dto.CarBrandResponse;
import ru.nlmk.carcatalogservice.model.CarBrand;

import java.util.List;

@Mapper
public interface CarBrandMapper {

    CarBrandMapper INSTANCE = Mappers.getMapper(CarBrandMapper.class);

    CarBrandResponse carBrandToCarBrandResponse(CarBrand carBrand);

    List<CarBrandResponse> carBrandToCarBrandResponse(List<CarBrand> carBrand);

}
