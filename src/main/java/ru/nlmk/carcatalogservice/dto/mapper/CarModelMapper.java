package ru.nlmk.carcatalogservice.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.nlmk.carcatalogservice.dto.CarModelResponse;
import ru.nlmk.carcatalogservice.model.CarModel;

import java.util.List;

@Mapper
public interface CarModelMapper {

    CarModelMapper INSTANCE = Mappers.getMapper(CarModelMapper.class);

    @Mapping(source = "carBrand.id", target = "brandId")
    CarModelResponse carModelToCarModelResponse(CarModel carModel);

    List<CarModelResponse> carModelToCarModelResponse(List<CarModel> carModel);

}
