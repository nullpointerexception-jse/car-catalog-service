package ru.nlmk.carcatalogservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nlmk.carcatalogservice.dto.CarModelResponse;
import ru.nlmk.carcatalogservice.service.CarModelService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class CarModelController {

    private final CarModelService carModelService;

    @Autowired
    public CarModelController(CarModelService carModelService) {

        this.carModelService = carModelService;
    }

    @GetMapping(value = "/models/{brandId}")
    public ResponseEntity<List<CarModelResponse>> getModelsByBrandId(@PathVariable Long brandId) {
        List<CarModelResponse> carModelResponses = carModelService.findByBrandId(brandId);
        if (carModelResponses.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .build();
        }
        return ResponseEntity.ok()
                .body(carModelResponses);
    }

}
