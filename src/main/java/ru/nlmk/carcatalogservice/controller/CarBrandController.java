package ru.nlmk.carcatalogservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nlmk.carcatalogservice.dto.CarBrandResponse;
import ru.nlmk.carcatalogservice.service.CarBrandService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class CarBrandController {

    private final CarBrandService carBrandService;

    @Autowired
    public CarBrandController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping(value = "/brands")
    public ResponseEntity<List<CarBrandResponse>> getAllBrands() {
        return ResponseEntity.ok()
                .body(carBrandService.findAll());
    }

}
