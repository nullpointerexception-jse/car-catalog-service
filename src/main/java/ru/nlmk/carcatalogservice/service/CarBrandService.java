package ru.nlmk.carcatalogservice.service;

import ru.nlmk.carcatalogservice.dto.CarBrandResponse;

import java.util.List;

public interface CarBrandService {

    List<CarBrandResponse> findAll();

}
