package ru.nlmk.carcatalogservice.service;

import ru.nlmk.carcatalogservice.dto.CarModelResponse;

import java.util.List;

public interface CarModelService {

    List<CarModelResponse> findByBrandId(Long id);

}
