package ru.nlmk.carcatalogservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.carcatalogservice.dto.CarBrandResponse;
import ru.nlmk.carcatalogservice.dto.mapper.CarBrandMapper;
import ru.nlmk.carcatalogservice.repository.CarBrandRepository;
import ru.nlmk.carcatalogservice.service.CarBrandService;

import java.util.List;

@Service
public class CarBrandServiceImpl implements CarBrandService {

    private static final CarBrandMapper mapper = CarBrandMapper.INSTANCE;
    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrandResponse> findAll() {
        return mapper.carBrandToCarBrandResponse(carBrandRepository.findAll());
    }

}
