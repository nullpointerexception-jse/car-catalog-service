package ru.nlmk.carcatalogservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.carcatalogservice.dto.CarModelResponse;
import ru.nlmk.carcatalogservice.dto.mapper.CarModelMapper;
import ru.nlmk.carcatalogservice.repository.CarModelRepository;
import ru.nlmk.carcatalogservice.service.CarModelService;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {

    private static final CarModelMapper mapper = CarModelMapper.INSTANCE;
    private final CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;

    }

    @Override
    public List<CarModelResponse> findByBrandId(Long id) {
        return mapper.carModelToCarModelResponse(carModelRepository.findByCarBrandId(id));
    }

}
