package ru.nlmk.carcatalogservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car_brand", schema = "public")
public class CarBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_brand_seq")
    @SequenceGenerator(name = "car_brand_seq", sequenceName = "car_brand_seq", schema = "public", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "carBrand", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<CarModel> carModels;

}
