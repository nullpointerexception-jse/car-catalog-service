package ru.nlmk.carcatalogservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.carcatalogservice.model.CarModel;

import java.util.List;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {

    List<CarModel> findByCarBrandId(Long id);

}
