package ru.nlmk.carcatalogservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.carcatalogservice.model.CarBrand;

public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {
}
