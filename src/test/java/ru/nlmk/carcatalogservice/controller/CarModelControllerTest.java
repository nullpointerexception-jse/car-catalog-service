package ru.nlmk.carcatalogservice.controller;

import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CarModelControllerTest extends AbstractController {

    @Test
    void getModelsByBrandId() throws Exception {
        mockMvc.perform(get("/models/1"))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/json"))
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.[0].id").value("1"))
                .andExpect(jsonPath("$.[0].name").value("R8"))
                .andExpect(jsonPath("$.[0].brandId").value("1"))
                .andExpect(jsonPath("$.[1].id").value("2"))
                .andExpect(jsonPath("$.[1].name").value("A8"))
                .andExpect(jsonPath("$.[1].brandId").value("1"));
    }

    @Test
    void modelNotFoundByBrandId() throws Exception {
        mockMvc.perform(get("/models/1111"))
                .andExpect(status().is4xxClientError());
    }

}